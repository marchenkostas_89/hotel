<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymicname
 * @property string $date_birth
 * @property string $added
 *
 * @property Order $order
 */
class Client extends \yii\db\ActiveRecord
{	
	public $orders_count;
	
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'patronymicname', 'date_birth'], 'required', 'message' => 'Это поле должно быть заполнено'],
            [['date_birth'], 'safe'],
			//['date_birth', 'date', 'format' => 'yyyy-MM-dd'],
            [['firstname', 'lastname', 'patronymicname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'patronymicname' => 'Отчество',
            'date_birth' => 'Дата рождения',
            'added' => 'Added',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['client_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }
}
