<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Client]].
 *
 * @see Client
 */
class ClientQuery extends \yii\db\ActiveQuery
{
	public function addOrdersCount()
	{
		return $this -> select('`client`.*, count(`order`.`id`) as orders_count') -> leftJoin('order', '`client`.`id` = `order`.`client_id`') -> addGroupBy('`client`.`id`') -> addOrderBy('orders_count desc');
	}
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Client[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Client|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}