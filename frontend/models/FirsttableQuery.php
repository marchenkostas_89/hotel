<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Firsttable]].
 *
 * @see Firsttable
 */
class FirsttableQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Firsttable[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Firsttable|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}