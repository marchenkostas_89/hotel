<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "firsttable".
 *
 * @property integer $id
 * @property string $book
 * @property string $author
 */
class Firsttable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'firsttable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book', 'author'], 'required'],
            [['book', 'author'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book' => 'Book',
            'author' => 'Author',
        ];
    }

    /**
     * @inheritdoc
     * @return FirsttableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FirsttableQuery(get_called_class());
    }
}
