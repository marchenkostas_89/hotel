<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RoomType]].
 *
 * @see RoomType
 */
class RoomTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RoomType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RoomType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
	
	public function enabled()
	{
		$this -> andWhere('enabled = 1');
		return $this;
	}
}