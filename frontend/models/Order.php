<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $date_order
 * @property integer $client_id
 * @property integer $room_type_id
 * @property integer $status_id
 * @property string $date_booking
 * @property integer $duration
 *
 * @property Status $status
 * @property Client $client
 * @property RoomType $roomType
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_order', 'date_booking'], 'safe'],
            [['room_type_id', 'date_booking', 'duration'], 'required'],
            [['client_id', 'room_type_id', 'status_id', 'duration'], 'integer'],
            [['status_id'], 'default', 'value' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_order' => 'Date Order',
            'client_id' => 'Client ID',
            'room_type_id' => 'Room Type ID',
            'status_id' => 'Status ID',
            'date_booking' => 'Дата',
            'duration' => 'Количество ночей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(RoomType::className(), ['id' => 'room_type_id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
