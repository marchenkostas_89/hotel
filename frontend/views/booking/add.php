<?php
	$this->title = 'Бронирование номера';
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
?>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?php echo $form->field($client, 'firstname') ?>
                <?php echo $form->field($client, 'lastname') ?>
                <?php echo $form->field($client, 'patronymicname') ?>
                <?php echo $form->field($client, 'date_birth') ?>
                
                <?php echo $form->field($order, 'date_booking') ?>
                <?php echo $form->field($order, 'duration') ?>

                <div class="form-group">
                    <?php echo Html::submitButton('Заказать', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>    