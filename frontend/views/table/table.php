<?php
	$this->title = 'Инфа о клиентах';
	use yii\helpers\Html;
	use yii\widgets\LinkPager;
?>

<table class="table">
	<tr>
    	<th>ФИО</th>
    	<th>дата</th>
    	<th>номер</th>
    	<th>Количество дней</th>
    </tr>
    <?php 
		foreach($orders as $order)
		{
			?>
            <tr>
            	<td><?php echo $order -> client -> firstname . ' ' . $order -> client -> lastname . ' ' . $order -> client -> patronymicname;?></td>
            	<td><?php echo $order -> date_booking;?></td>
            	<td><?php echo $order -> roomType-> title;?></td>
            	<td><?php echo $order -> duration;?></td>
            </tr>
            <?php
		}
	?>
</table>

<div style="margin-top:150px;">
    <table class="table">
        <tr>
            <th>ФИО</th>
            <th>Количество заказов</th>
        </tr>
        <?php 
            foreach($clients as $client)
            {
                ?>
                <tr>
                    <td><?php echo $client -> firstname . ' ' . $client -> lastname . ' ' . $client -> patronymicname;?></td>
                    <td><?php echo $client -> orders_count;?></td>
                </tr>
                <?php
            }
        ?>
    </table>
    <?php echo LinkPager::widget(['pagination' => $pagination]);?>
</div>