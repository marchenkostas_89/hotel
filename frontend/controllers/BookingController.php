<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\RoomType;
use app\models\RoomTypeQuery;
use app\models\Client;
use app\models\ClientQuery;
use app\models\Order;
use app\models\OrderQuery;


class BookingController extends Controller
{
	private function showForm($client)
	{
		if($_POST)
		{
			$client -> attributes = $_POST['Client'];
			if($client -> save())
				{return $this -> render('success', ['idClient' => $client -> id]);}				
		}
		
		return $this -> render('index', array('client' => $client));		
	}
	
	
	
	
	public function actionIndex($roomType)
	{
		$client = new Client();	
		$order = new Order();
		if(($_POST) && isset($_POST['Client']) && isset($_POST['Order']))
		{
			$client -> attributes = $_POST['Client'];
			$orderData = $_POST['Order'];
			$orderData['room_type_id'] = $roomType;	
			$order -> attributes = $orderData;	
			$transaction = \Yii::$app -> db -> beginTransaction();	
			try
			{							
				if($order -> validate() && $client -> save())
				{
					$order -> client_id = $client -> id;				
					if($order -> save())
					{
						$transaction -> commit();
						return $this -> render('success');
					}								
				}
			}
			catch(Exception $e)
			{
				$transaction -> rollback();
				throw $e;
			}
		}
		
		return $this  -> render('add', ['client' => $client, 'order' => $order]);
	}
	
	
	
	
	
	public function actionEdit($idClient)
	{
		//$client = Client::find() -> andWhere("id = :id") -> addParams(["id" => $idClient]) -> one();  
		$client = Client::findOne($idClient);
		if(!$client)
			{throw new \yii\web\NotFoundHttpException;}
		return $this -> showForm($client);
	}
}
