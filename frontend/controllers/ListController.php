<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Firsttable;
use app\models\FirsttableQuery;


class ListController extends Controller
{
	public function actionIndex()
	{
		$list = Firsttable :: find() -> all();		
		return $this -> render('index', array('list' => $list));
	}	
}