<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\RoomType;
use app\models\RoomTypeQuery;
use app\models\Order;
use app\models\OrderQuery;

class RoomController extends Controller
{
	public function actionRoom($id)
	{
		$room = RoomType::findOne($id);
		return $this -> render('room', ['room' => $room]);
	}
}