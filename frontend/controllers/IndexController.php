<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\RoomType;
use app\models\RoomTypeQuery;


class IndexController extends Controller
{
	public function actionIndex()
	{
		$roomsTypes = RoomType :: find() -> enabled() -> addOrderBy('price desc') -> all();
		return $this -> render('index', array('roomsTypes' => $roomsTypes));		
	}
}