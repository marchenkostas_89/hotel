<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Client;
use app\models\ClientQuery;
use app\models\Order;
use app\models\OrderQuery;
use yii\data\Pagination;

class TableController extends Controller
{
	public function actionTable()
	{
		$orders = Order::find() -> andWhere('status_id = 1') -> with(['client', 'roomType']) ->addOrderBy('date_order') -> all();
		$clients_count = Client::find() -> count();
		$pagination = new Pagination(['totalCount' => $clients_count, 'pageSize' => 4]);
		$clients = Client::find() -> addOrdersCount() -> offset($pagination -> offset) -> limit($pagination -> limit) -> all();
		return $this -> render('table', ['orders' => $orders, 'clients' => $clients, 'pagination' => $pagination]);
	}
}