<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\RoomType;
use app\models\RoomTypeQuery;
use app\models\Order;
use app\models\OrderQuery;

class OrderController extends Controller
{
	public function actionIndex()
	{
		return $this -> render('index');
	}
	
	public function actionOrder()
	{
		return $this -> render('order');
	}
}